# OOP Basics + Typescript basics

## Теоретический блок по основам ООП

### Рекомендации

Рекомендуется внимательно изучить материалы из данного блока. На основании данных матералов будет реализовано следующее практическое задание и дипломный проект

## Материалы для изучения по ООП

- [4 базовых принципа ООП](https://habr.com/ru/companies/ruvds/articles/665290/) - советую запомнить, очень часто спрашивают на собеседованиях
- [Контекст this](https://github.com/azat-io/you-dont-know-js-ru/blob/master/this%20%26%20object%20prototypes/ch2.md) - подробное объяснение как работает контекст this, эта глава настоятельно рекомендуется к прочтению, тема очень часто спрашивается на собеседовании
- [Функции-конструкторы](https://learn.javascript.ru/constructor-new) - полезно изучить чтобы понять как работают классы под капотом
- [Классы](https://learn.javascript.ru/classes) - подробный разбор как устроены классы в JS, достаточно изучить разделы 1-4
- [Прототипы и наследование](https://learn.javascript.ru/prototypes) - подробная информация как работает прототипное наследование
- [Разница между proto и prototype](https://www.youtube.com/watch?v=b55hiUlhAzI) - лучшее объяснение как работает прототипное наследование в классах JS
- [Паттерны проектирования](https://www.youtube.com/watch?v=RyY6x_6ws4s&list=PLNkWIWHIRwMGzgvuPRFkDrpAygvdKJIE4) - плейлист в котором приведены основные паттерны проектирования. Рекомендуется посмотреть введение, а также паттерны singleton, observer, фабрика
- [Паттерн eventbus](https://www.thisdot.co/blog/how-to-implement-an-event-bus-in-typescript) - статья с примерами реализации паттерна Eventbus. Настоятельно рекомендуется к изучению, т.к. этот паттерн активно используется в работе

## Материалы для изучения по TS

- [Краткий курс по TS](https://www.youtube.com/watch?v=MtO76yEYbxA&list=PLNkWIWHIRwMEm1FgiLjHqSky27x5rXvQa) - этого курса для начала достаточно для использования typescript в проекте
- [Документация по TS](https://www.typescriptlang.org/docs/)
- [Документация на русском](https://scriptdev.ru/guide/)
- [Инструкция по подключению TS в проект](https://proglib.io/p/migriruem-s-javascript-na-typescript-bystro-i-bezboleznenno-2020-01-19)